package sk.fiit.ui.gui.controllers;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import sk.fiit.ui.core.Core;
import sk.fiit.ui.core.loader.Loader;
import sk.fiit.ui.core.memories.Memory;
import sk.fiit.ui.core.structures.Action;
import sk.fiit.ui.core.structures.Predicate;
import sk.fiit.ui.core.structures.Rule;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class LayoutController implements Initializable {

    @FXML
    private TextArea debugTArea;
    @FXML
    private Button stepBtn;
    @FXML
    private TextArea rulesTArea;
    @FXML
    private TextArea predicatesTArea;
    @FXML
    private MenuItem newMI;
    @FXML
    private MenuItem closeMI;
    @FXML
    private Button rewriteBtn;
    @FXML
    private Button solveBtn;
    @FXML
    private MenuItem predicatesMI;
    @FXML
    private TextArea outputTArea;
    @FXML
    private MenuItem rulesMI;
    
    private FileChooser fileChooser= new FileChooser(); 
    
    private Loader loader = new Loader();
    private Core core = new Core();
    @FXML
    void rewritePredicates(ActionEvent event) {
    	String[] allOfThem = predicatesTArea.getText().split("\n");
    	loader.rewritePredicates(allOfThem);
    	printPredicates();
    }

    @FXML
    void stepForward(ActionEvent event) {
    	if (core.createPredicates()) {
			String tmpA = Memory.getMemory().debug.peek();
			outputTArea.appendText(Action.actionToMemory(tmpA));
    	}
    	printDebug();
    	printPredicates();
    }
    
    private void printDebug() {
    	String output = "";
    	for (String s : Memory.getMemory().debug) {
    		output += s+"\n";
    	}
    	debugTArea.setText(output);
    }

    @FXML
    void solve(ActionEvent event) {
    	long start = System.currentTimeMillis();
    	while (core.createPredicates()) {
    		String tmpA = Memory.getMemory().debug.poll();
    		outputTArea.appendText(Action.actionToMemory(tmpA));
    	}
    	printPredicates();
    	System.out.println(System.currentTimeMillis()-start);
    }

    @FXML
    void newBeginning(ActionEvent event) {
    	predicatesTArea.clear();
    	rulesTArea.clear();
    	debugTArea.clear();
    	outputTArea.clear();
    	loader.clear();
    	rulesMI.setDisable(true);
    }

    @FXML
    void closeProgram(ActionEvent event) {
    	System.exit(0);
    }

    @FXML
    void loadPredicates(ActionEvent event) {

    	fileChooser.setTitle("Open predicates file");
    	File file = fileChooser.showOpenDialog(new Stage());
    	if (file != null) {
    		fileChooser.setInitialDirectory(file.getParentFile());
    		loader.readPredicates(file);
    		printPredicates();
    		rulesMI.setDisable(false);
    	}
    }

    private void printPredicates() {
		String output = "";
    	for (Predicate p : Memory.getMemory().predicates) {
			output += p.toString() +"\n";
		}
    	predicatesTArea.setText(output);
	}

	@FXML
    void loadRules(ActionEvent event) {
		fileChooser.setTitle("Open rules file");
    	File file = fileChooser.showOpenDialog(new Stage());
    	if (file != null) {
    		fileChooser.setInitialDirectory(file.getParentFile());
    		loader.readRules(file);
    		printRules();    		
    	}
    }

	private void printRules() {
		String output = "";
		for (Rule r : Memory.getMemory().rules) {
			output += r.toString();
		}
		rulesTArea.setText(output);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		rulesMI.setDisable(true);
	}

}
