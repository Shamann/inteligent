package sk.fiit.ui.core.structures;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

public class Rule {

	private String name;
	private List<Condition> conditions = new LinkedList<Condition>();
	private List<Action> actions = new LinkedList<Action>();
	private List<Hashtable<String, String>> filledVariables = new LinkedList<Hashtable<String,String>>();
	
	public Rule(String name) {
		this.name = name.replace(":", "");
	}
	
	protected static List<String> findVariablesInPattern(String pattern) {
		List<String> output = new ArrayList<String>(5);
		
		for (int i = 0; i < pattern.length(); i++) {
			if (pattern.getBytes()[i]=='?') {
				int from = i;
				int to = pattern.length();
				for (int j = i; j < pattern.length(); j++) {
					if (pattern.getBytes()[j]==' ') {
						to = j;
						break;
					}
				}
				output.add(pattern.substring(from, to));
			}
		}
		
		return output;
	}
	
	public String getName() {
		return name;
	}

	public void setConditions(String parsableConditions) {
		String body = parsableConditions.replaceAll("(^.* \\()","");
		
		String[] conditions = body.split("\\)\\(");
		for (int i = 0; i < conditions.length; i++) {
			conditions[i] = conditions[i].replace("(", "");
			conditions[i] = conditions[i].replaceAll("(\\(){0,}(.*)(\\)){2,}", "$2");
			this.conditions.add(new Condition(conditions[i]));
		}
		
	}

	public void setActions(String parsableActions) {
		
		String body = parsableActions.replaceAll("(^.* \\()", "");
		
		String[] actions = body.split("\\)\\(");
		for (int i = 0; i < actions.length; i++) {
			actions[i] = actions[i].replace("(", "");
			actions[i] = actions[i].replaceAll("(\\(){0,}(.*)(\\)){2,}", "$2");
			this.actions.add(new Action(actions[i]));
		}
		
	}
	
	public List<String> createActionList() {
		LinkedList<String> actionList = new LinkedList<String>();
		for (Hashtable<String, String> ht : filledVariables) {
			String acts = name;
			for (int i=0; i < actions.size(); i++) {
				String tmp = actions.get(i).action(ht);
				if (!tmp.isEmpty()) {
					acts += ("," + tmp);
				}
			}
			if (acts.contains("pridaj") || acts.contains("vymaz")) {
				actionList.add(acts);
			}
		}
		filledVariables.clear();
		return actionList;
	}

	public boolean checkPredicates() {
		for (Condition c : conditions) {
			c.checkPredicate(filledVariables);
			if (filledVariables.isEmpty()) {
				break;
			}
		}
		if (filledVariables.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public String toString() {
		String output = "Meno: " + name + "\nAK    ";
		int i = 0;
		for (Condition cond : conditions) {
			++i;
			output += cond.toString();
			if (conditions.size()>1 && i<conditions.size()) {
				output += ",";
			}
		}
		i = 0;
		output += "\nPOTOM ";
		for (Action act : actions) {
			++i;
			output += act.getAction();
			if (actions.size() > 1 && i<actions.size()) {
				output += ",";
			}
		}
		output += "\n\n";
		
		return output;
		
	}
}
