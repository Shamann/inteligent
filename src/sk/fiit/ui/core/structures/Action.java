package sk.fiit.ui.core.structures;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import sk.fiit.ui.core.memories.Memory;

public class Action {

	public static final int ADD = 1;
	public static final int DELETE = 2;
	public static final int MESSAGE = 3;
	
	private final int type;
	private String head;
	private String origin;
	private String body;
	private List<String> variables = new ArrayList<String>(5);
	
	/**
	 * makes action from given String pattern
	 * @param pattern - in format without quotations: head body
	 */
	public Action(String pattern) {
		origin = pattern;
		head = origin.split(" ")[0];
		if (head.equalsIgnoreCase("pridaj")) {
			type = ADD;
		} else  if (head.equalsIgnoreCase("vymaz")) {
			type = DELETE;
		} else {
			type = MESSAGE;
		}
		
		body = pattern.substring(head.length()+1);
		variables = Rule.findVariablesInPattern(body);
	}
	
	public int getType() {
		return type;
	}
	
	@Override
	public String toString() {
		String output = origin + " -";
		for (String s : variables) {
			output += " " + s;
		}
		return output;
	}
	
	public String getAction() {
		return origin;
	}
	
	/**
	 * Creates actual action for given rule in string format with its header<br>
	 * 
	 * @param vars loaded and filled variables in Hash table
	 * @return empty String if the action is unnecessary, else new predicate in format: action body-of-predicate
	 */
	public String action(Hashtable<String, String> vars) {
		String output = new String(body);
		
		for (String s : variables) {
			output = output.replace(s, vars.get(s));
		}
		if (type == ADD) {
			for (Predicate p : Memory.getMemory().predicates) {
				if (p.toString().equalsIgnoreCase(output)) {
					return "";
				}
			}
			output = head + " " + output;
			return output;
		} else
		if (type == DELETE) {
			for (Predicate p : Memory.getMemory().predicates) {
				if (p.toString().equalsIgnoreCase(output)) {
					output = head + " " + output;
					return output;
				}
			}
			return "";
		} else {
			output = head + " " + output;
			return output;
		}
	}
	
	public static String actionToMemory(String pattern) {
		String output = "";
		String[] action = pattern.split(",");
		for (int i = 1; i < action.length; i++) {
			if (action[i].split(" ")[0].equalsIgnoreCase("pridaj")) {
				Memory.getMemory().predicates.add(new Predicate(action[i].replace("pridaj ", "")));
			} else if  (action[i].split(" ")[0].equalsIgnoreCase("vymaz")) {
				int index = 0;
				for (Predicate p : Memory.getMemory().predicates) {
					if (action[i].contains(p.toString())) {
						Memory.getMemory().predicates.remove(index);
						index--;
						break;
					}
					index++;
				}
			} else {
				output += action[i].replace("sprava ", "") + "\n";
			}
		}
		return output;
	}
}
