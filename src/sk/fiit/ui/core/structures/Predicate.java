package sk.fiit.ui.core.structures;

import java.util.Hashtable;

public class Predicate {

	private String origin;
	private Hashtable<Integer, String> arguments;
	private int size;
	
	public Predicate(String fulltext) {
		
		String regex = "(^\\()(.*)(\\)$)"; 
		origin = fulltext.replaceAll(regex, "$2");
		
		this.arguments = new Hashtable<Integer, String>();
		String[] arguments = origin.split(" ");
		for (int i = 0; i < arguments.length; i++) {
			this.arguments.put(i+1, arguments[i]);
		}
		size = this.arguments.size();
	}
	
	public String toString() {
		return origin;
	}
	
	public int size() {
		return size; 
	}
	
	public Hashtable<Integer, String> getArgumentsTable() {
		return arguments;
	}
	
	public String getArgument(int index) {
		return arguments.get(index);
	}
}
