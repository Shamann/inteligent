package sk.fiit.ui.core.structures;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import sk.fiit.ui.core.memories.Memory;

public class Condition {

	private String origin;
	private boolean special = false;
	private String head;
	private List<String> variables = new ArrayList<String>(5);
	private Hashtable<String, Integer> words = new Hashtable<String, Integer>();
	private Hashtable<Integer, String> pattern = new Hashtable<Integer, String>();
	private int wordCount;
	
	public Condition(String pattern) {
		origin = pattern;
		variables = Rule.findVariablesInPattern(origin);
		
		String[] arguments = origin.split(" ");
		
		for (int i = 0; i < arguments.length; i++) {
			this.words.put(arguments[i], i+1);
			if (!arguments[i].contains("?")) {
				this.pattern.put(i+1, arguments[i]);
			}
		}
		head = this.pattern.get(1);
		if (head!=null && head.matches("<>|==|=|<|>")) {
			special = true;
		}
		wordCount = words.size();
	}

	public boolean isSpecial() {
		return special;
	}
	
	public String getCondition() {
		return origin;
	}
	
	public Hashtable<String, Integer> getWordHashtable() {
		return words;
	}
	
	public List<String> getVariables() {
		return variables;
	}
	
	public int size() {
		return wordCount;
	}
	
	@Override
	public String toString() {
		return origin;
	}

	public void checkPredicate(List<Hashtable<String, String>> filledVariables) {
		if (special) {
				switch (head) {
				case "<>" : for (int i = 0; i < filledVariables.size(); i++) {
								if (filledVariables.get(i).get(variables.get(0)).equalsIgnoreCase(
										filledVariables.get(i).get(variables.get(1)))) {
									filledVariables.remove(i);
									--i;
								}
							}	
							break;
				case "==" : for (int i = 0; i < filledVariables.size(); i++) {
								if (!filledVariables.get(i).get(variables.get(0)).equalsIgnoreCase(
										filledVariables.get(i).get(variables.get(1)))) {
									filledVariables.remove(i);
									--i;
								}	
							}
							break;
				}
		} else {
			// if not special condition
			Hashtable<String, String> output = new Hashtable<String, String>();
			// first condition - empty dictionary of variables
			if (filledVariables.isEmpty()) {
				// condition is checked on every predicate to find corresponding ones
				for (Predicate predicate : Memory.getMemory().predicates) {
					boolean canAdd = true;
					// checks size and words of predicates and condition
					if (predicate.size() != size()) {
						canAdd = false;
					} else {
						for (Integer index : pattern.keySet()) {
							if (!pattern.get(index).equalsIgnoreCase(predicate.getArgument(index))) {
								canAdd = false;
								break;
							}
						}
					}
					// adds into field of variable values
					if (canAdd) {
						for (String var : variables) {
							output.put(var, predicate.getArgument(words.get(var)));
						}
						filledVariables.add(new Hashtable<String, String>(output));
					}
					output.clear();
				}
			} else {
				List<Hashtable<String, String>> smallOne = new LinkedList<Hashtable<String,String>>();
				// condition is checked on every predicate to find corresponding ones
				for (Predicate predicate : Memory.getMemory().predicates) {
					boolean canAdd = true;
					// checks size and words of predicates and condition
					if (predicate.size() != size()) {
						canAdd = false;
					} else {
						for (Integer index : pattern.keySet()) {
							if (!pattern.get(index).equalsIgnoreCase(predicate.getArgument(index))) {
								canAdd = false;
								break;
							}
						}
					}
					// adds into field of variable values
					if (canAdd) {
						for (String var : variables) {
							output.put(var, predicate.getArgument(words.get(var)));
						}
						smallOne.add(new Hashtable<String, String>(output));
					}
					output.clear();
				}
				List<Hashtable<String, String>> aux = new LinkedList<Hashtable<String,String>>();
				for (int i = 0; i < filledVariables.size(); i++) {
					Hashtable<String, String> temporary = filledVariables.remove(i);
					--i;
					for (Hashtable<String, String> htS : smallOne) {
						boolean canAdd = true;
						for (String var : htS.keySet()) {
							if (temporary.get(var)!=null && !htS.get(var).equalsIgnoreCase(temporary.get(var))) {
								canAdd = false;
								break;
							}
						}
						if (canAdd) {
							Hashtable<String, String> tempNew = new Hashtable<String, String>(temporary);
							for (String var : variables) {
								if (tempNew.get(var)==null) {
									tempNew.put(var, htS.get(var));
								}
							}
							aux.add(tempNew);
						}
					}	
				}
				filledVariables.addAll(aux);
			}
		}
	}
}
