package sk.fiit.ui.core;

import java.util.List;

import sk.fiit.ui.core.memories.Memory;
import sk.fiit.ui.core.structures.Rule;

public class Core {

	public boolean createPredicates() {
		Memory.getMemory().debug.clear();
		
		for (Rule rule : Memory.getMemory().rules) {
			if (rule.checkPredicates()) {
				List<String> tmp = rule.createActionList();
				if (!tmp.isEmpty()) {
					Memory.getMemory().debug.addAll(tmp);
				}
			} else {
				continue;
			}
		}
		if (Memory.getMemory().debug.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

}
