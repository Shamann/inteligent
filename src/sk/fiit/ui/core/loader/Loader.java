package sk.fiit.ui.core.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import sk.fiit.ui.core.memories.Memory;
import sk.fiit.ui.core.structures.Predicate;
import sk.fiit.ui.core.structures.Rule;

public class Loader {
	
	public void readPredicates(File file)  {
		BufferedReader buffer = null;
		try {
			
		buffer = new BufferedReader(new FileReader(file));
		String line = "";
		Memory.getMemory().predicates.clear();
			while ((line = buffer.readLine()) != null) {
				Memory.getMemory().predicates.add(new Predicate(line));
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				buffer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void readRules(File file) {
		BufferedReader buffer = null;
		try {
			
			buffer = new BufferedReader(new FileReader(file));
			String line = "";
			
			Memory.getMemory().rules.clear();
				while ((line = buffer.readLine()) != null) {
					if (!line.equalsIgnoreCase("")) {
						Rule rule = new Rule(line);
						line = buffer.readLine();
						rule.setConditions(line);
						line = buffer.readLine();
						rule.setActions(line);
						Memory.getMemory().rules.add(rule);
					}
				}
			
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					buffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		
	}

	public void clear() {
		Memory.getMemory().debug.clear();
		Memory.getMemory().rules.clear();
		Memory.getMemory().predicates.clear();
	}

	public void rewritePredicates(String[] predicates) {
		Memory.getMemory().predicates.clear();
		for (int i = 0; i < predicates.length; i++) {
			Memory.getMemory().predicates.add(new Predicate(predicates[i]));
		}
		
	}
}
