package sk.fiit.ui.core.memories;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import sk.fiit.ui.core.structures.Predicate;
import sk.fiit.ui.core.structures.Rule;

public class Memory {

	
	private static final Memory m = new Memory();
	
	public List<Predicate> predicates = new ArrayList<Predicate>(30);
	public List<Rule> rules = new ArrayList<Rule>(30);
	public LinkedList<String> debug = new LinkedList<String>();

	private Memory() {

	}
	
	public static Memory getMemory() {
		return m;
	}
}
